

all: $(patsubst %.dbk,%.man,$(wildcard *.dbk))

%.man: %.dbk
	docbook2x-man --to-stdout $< > $@

clean:
	rm -f *.man *~
